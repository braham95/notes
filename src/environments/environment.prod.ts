export const API_ROOT_URL = 'https://localhost:4200'; // TODO: to change to the real API addresse
export const API_URL = `${API_ROOT_URL}/api`;
export const environment = {
  production: true,
  ENDPOINTS: {
    NOTES_URL: `${API_URL}/notes`,
  }
};
