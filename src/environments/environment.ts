// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
export const API_ROOT_URL = 'https://localhost:4200'; // TODO: to change to the real API addresse
export const API_URL = `${API_ROOT_URL}/api`;
export const environment = {
  production: false,
  ENDPOINTS: {
    NOTES_URL: `https://run.mocky.io/v3/4c9d2033-05bb-46d4-befc-b141e07e1bcf`,
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
