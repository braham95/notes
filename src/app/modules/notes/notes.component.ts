import { NgxIndexedDBService } from 'ngx-indexed-db';
import { InitalConfig } from './../../shared/models/initial-config.model';
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { note } from 'src/app/shared/models/note.model';
import { user } from 'src/app/shared/models/user.model';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { NotifierService } from 'src/app/services/notifier.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class NotesComponent implements OnInit, OnDestroy {
  user = new user();
  notes: note[] = [];
  showDivider = false;
  selectedNoteId: number = 0;
  notesInitialConfig = new InitalConfig();
  filtredNotes: note[] = [];
  searchNotesControl = new FormControl();
  addedSub: Subscription = new Subscription;
  editedSub: Subscription = new Subscription;
  deletedSub: Subscription = new Subscription;
  constructor(
    private _router: Router,
    private _notifierService: NotifierService,
    private dbService: NgxIndexedDBService
  ) {
    this.selectedNoteId = +_router.url.split('/')[2] ?? 0;
  }

  ngOnInit(): void {
    // get the user's notes
    this.getUserNotes();

    // manage events
    //TODO: only because there is no backend or time to use ngrx to share notesList variable
    this.manageEvents();
  }

  ngOnDestroy(): void {
    this.addedSub.unsubscribe();
    this.deletedSub.unsubscribe();
    this.editedSub.unsubscribe();
  }

  /**
   * manage events
   */
  manageEvents() {
    this.addedSub = this._notifierService.addedNote$.subscribe(note => {
      this.notes.unshift(note);
    });
    this.deletedSub = this._notifierService.deletedNote$.subscribe(noteId => {
      if (this.notes.find(oldNote => oldNote.id === noteId)) {
        this.notes.splice(this.notes.indexOf(this.notes.find(oldNote => oldNote.id === noteId)!), 1);
      }
    });
    this.editedSub = this._notifierService.editedNote$.subscribe(note => {
      if (this.notes.find(oldNote => oldNote.id === note.id)) {
        this.notes.splice(this.notes.indexOf(this.notes.find(oldNote => oldNote.id === note.id)!), 1, note);
      }
    });
  }

  /**
   * toggle Search Field showing status
   */
  toggleSearchField(): void {
    this.showDivider = !this.showDivider;
  }

  /**
   * on selecte note
   * @param note the selected note
   */
  selectNote(note: any): void {
    this.selectedNoteId = note.id;
    this._router.navigate(['notes', this.selectedNoteId, 'view-note']);
  }

  /**
   * get user's notes
   */
  getUserNotes(): void {
     // TODO: add api getAllNotes call later with backend and concat with the saved ones
    this.dbService.getAll('notes').subscribe(
      {
        next: (response) => { this.setData(response) },
        error: (err: any) => {
          console.error('something went wrong' + err);
        }
      }
    );
  }

  /**
   * set data
   * @param data data
   */
  setData(data: any): void {
    this.notes = data;
    this.filtredNotes = this.notes;
    if (this.notes.length && !this.selectedNoteId) {
      this.selectedNoteId = this.notes[0].id;
      this._router.navigate(['notes', this.selectedNoteId]);
    }
  }

  /**
   * refresh notes
   */
  refresh(): void {
    this.initializeData();
    this.getUserNotes();
  }

  /**
   * initializeData
   */
  initializeData(): void {
    this.notes = [];
    this.notesInitialConfig = new InitalConfig();
    this.searchNotesControl.setValue(null);
  }

  /**
   * add new note
   */
  addNewNote(): void {
    this.selectedNoteId = 0;
    this._router.navigate(['notes', 'new-note']);
  }

  /**
   * filter notes
   */
  searchNotes() {
    // TODO: make it with the api
    this.filtredNotes = [...this.notes.filter(note =>
      note.title.toLowerCase().trim().includes(this.searchNotesControl.value?.toLowerCase().trim()))];
  }
}
