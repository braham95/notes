import { NgxIndexedDBService } from 'ngx-indexed-db';
import { note } from './../../../shared/models/note.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotesService } from 'src/app/services/notes.service';
import { NotifierService } from 'src/app/services/notifier.service';

@Component({
  selector: 'app-view-note',
  templateUrl: './view-note.component.html',
  styleUrls: ['./view-note.component.scss']
})
export class ViewNoteComponent implements OnInit {
  noteId: number = 0;
  note = new note();
  noteFormGroup: FormGroup | null = null;
  constructor(
    private _activedRoute: ActivatedRoute,
    private _router: Router,
    private _notifierService: NotifierService,
    private dbService: NgxIndexedDBService
  ) {
    this._activedRoute.params.subscribe(params => {
      this.noteId = +params['noteId'];
      if (this.noteId) {
        this.getNoteById(this.noteId);
      } else {
        console.error('no bote id');
        this._router.navigate(['notes']);
      }
    });
  }

  ngOnInit(): void {
  }

  /**
   * get selected note
   * @param noteId selected note
   */
  getNoteById(noteId: number) {
    this.dbService.getByKey('notes', noteId).subscribe({
      next: (response: any) => { this.setData(response) },
      error: (err: any) => {
        console.error('something went wrong' + err);
      }
    });
  }

  /**
   * set the sselected note
   * @param note the selected note
   */
  setData(note: note): void {
    this.note = note;
  }

  /**
   * edit selected note
   * @param noteId the note id
   */
  editNote(noteId: number): void {
    this._router.navigate(['notes', noteId, 'edit-note']);
  }

  /**
   * delete selected note
   * @param noteId the note id
   */
  deleteNote(note: note): void {
     // TODO: add api delete call later with backend
    const deleteStatus = confirm('Are you sure you want to delete this note?');
    if (deleteStatus) {
      this.dbService.delete('notes',note.id).subscribe(
        {
          next: (noteResult) => {
            alert('note deleted');
             // TODO: because there is no backend
             this._notifierService.deletedNoteFunc(note.id) // TODO:  THE noteResult
            this._router.navigate(['notes', 'new-note']); // TODO: GET THE ID FROM THE NEW note
          },
          error: (err: any) => {
            console.error('something went wrong' + err);
          }
        }
      );
    }
  }
  
}
