import { UpsertNoteComponent } from './upsert-note/upsert-note.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotesComponent } from './notes.component';
import { ViewNoteComponent } from './view-note/view-note.component';

const notesRoutes: Routes = [
  {
    path: '',
    component: NotesComponent,
    children: [
      {
        path: '',
        redirectTo: 'new-note',
        pathMatch: 'full'
      },
      {
        path: 'new-note',
        component: UpsertNoteComponent,
      },
      {
        path: ':noteId',
        children: [
          {
            path: '',
            redirectTo: 'view-note',
            pathMatch: 'full'
          },
          {
            path: 'edit-note',
            component: UpsertNoteComponent,
          },
          {
            path: 'view-note',
            component: ViewNoteComponent,
          }
        ]
      },

    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(notesRoutes)],
  exports: [RouterModule]
})
export class NotesRoutingModule { }
