import { UpsertNoteComponent } from './upsert-note/upsert-note.component';
import { ViewNoteComponent } from './view-note/view-note.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotesComponent } from './notes.component';
import { NotesRoutingModule } from './notes-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { NotesService } from 'src/app/services/notes.service';
import { HttpClientModule } from '@angular/common/http';
import { NotifierService } from 'src/app/services/notifier.service';

@NgModule({
  imports: [
    CommonModule,
    NotesRoutingModule,
    SharedModule,
  ],
  declarations: [
    NotesComponent,
    ViewNoteComponent,
    UpsertNoteComponent
  ],
  providers: [NotesService, NotifierService]
})
export class NotesModule { }
