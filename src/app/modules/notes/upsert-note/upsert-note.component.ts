import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { note } from 'src/app/shared/models/note.model';
import { NotesService } from 'src/app/services/notes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NotifierService } from 'src/app/services/notifier.service';
import { NgxIndexedDBService } from 'ngx-indexed-db';

@Component({
  selector: 'app-upsert-note',
  templateUrl: './upsert-note.component.html',
  styleUrls: ['./upsert-note.component.scss']
})
export class UpsertNoteComponent implements OnInit {
  noteId: number = 0;
  note = new note();
  noteFormGroup: FormGroup | null = null;
  constructor(
    private _activedRoute: ActivatedRoute,
    private _router: Router,
    private _formBuilder: FormBuilder,
    private _notifierService: NotifierService,
    private dbService: NgxIndexedDBService
  ) {
    this._activedRoute.params.subscribe(params => {
      this.noteId = +params['noteId'];
      if (this.noteId) {
        this.getNoteById(this.noteId);
      }
    });
  }

  ngOnInit(): void {
    this.initNoteFormGroup();
  }

  /**
   * init note form group
   */
  initNoteFormGroup() {
    this.noteFormGroup = this._formBuilder.group({
      title: this._formBuilder.control('', Validators.compose([Validators.required, Validators.maxLength(255)])),
      content: this._formBuilder.control('', Validators.compose([Validators.required, Validators.maxLength(1500)]))
    });
  }

  /**
   * get selected note
   * @param noteId selected note
   */
  getNoteById(noteId: number): void {
    this.dbService.getByKey('notes', noteId).subscribe({
      next: (response: any) => { this.setData(response) },
      error: (err: any) => {
        console.error('something went wrong' + err);
      }
    });
  }

  /**
   * set the sselected note
   * @param note the selected note
   */
  setData(note: note): void {
    this.note = note;
    this.noteFormGroup!.get('title')!.setValue(this.note.title);
    this.noteFormGroup!.get('content')!.setValue(this.note.content);
  }

  /**
   * validate form
   */
  validate(): void {
    if (this.noteFormGroup?.valid) {
      if (this.noteId) {
        this.updateNote(this.setNewNotePayload());
      } else {
        this.createNote(this.setNewNotePayload());
      }
    } else {
      this.noteFormGroup?.markAllAsTouched();
    }
  }


  /**
   * get new note pauload
   * @returns note
   */
  setNewNotePayload(): note {
    let newNote: note = new note();
    newNote.title = this.noteFormGroup!.get('title')!.value;
    newNote.content = this.noteFormGroup!.get('content')!.value;
    newNote.id = this.noteId ?? 12;
    return newNote;
  }

  /**
   * update note
   * @param newNote updated note
   */
  updateNote(newNote: note) {
     // TODO: add api update call later with backend
    this.dbService
      .update('notes',
        {
          id: this.noteId,
          title: newNote.title,
          content: newNote.content,
          creationDate: this.note.creationDate,
          updateDate: newNote.updateDate,
          saveDate: this.note.saveDate,
        }
      )
      .subscribe(
        {
          next: (noteResult: note) => {
            alert('note updated');
            this._notifierService.editedNoteFunc(noteResult);
            this._router.navigate(['notes', noteResult.id, 'view-note']);
          },
          error: (err: any) => {
            console.error('something went wrong' + err);
          }
        }
      );
  }

  /**
   * create note
   * @param newNote new note
   */
  createNote(newNote: note) {
    // TODO: add api create call later with backend
    this.dbService
      .add('notes',
        {
          title: newNote.title,
          content: newNote.content,
          creationDate: newNote.creationDate,
          updateDate: newNote.updateDate,
          saveDate: null,
        }
      )
      .subscribe(
        {
          next: (noteResult: note) => {
            alert('note created');
            this._notifierService.addedNoteFunc(noteResult);
            this._router.navigate(['notes', noteResult.id, 'view-note']);
          },
          error: (err: any) => {
            console.error('something went wrong' + err);
          }
        }
      );
  }
}
