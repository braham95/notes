import { InitalConfig } from './../shared/models/initial-config.model';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { note } from '../shared/models/note.model';

@Injectable({
    providedIn: 'root'
})
export class NotesService {
    constructor(private httpClient: HttpClient) { }

    /**
     * get user's notes
     * @param page page index
     * @param size page count
     * @param sort column to sort on
     * @param dir direction of the sort
     * @param search criteria of the search
     * @returns 
     */
    getNotes(notesInitialConfig: InitalConfig): Observable<any> {
        let queryParams = new HttpParams();
        queryParams = queryParams.append('page', notesInitialConfig.page.toString());
        queryParams = queryParams.append('size', notesInitialConfig.size.toString());
        queryParams = queryParams.append('sort', [notesInitialConfig.sort,
        notesInitialConfig.dir].toString());
        queryParams = notesInitialConfig.search ?
            queryParams.append('search', notesInitialConfig.search) : queryParams;
        const req = `${environment.ENDPOINTS.NOTES_URL}`;
        return this.httpClient.get(req, { params: queryParams, observe: 'response' });
    }

    /**
     * get note by id
     * @param noteId note ID
     * @returns 
     */
    getNoteById(noteId: number): Observable<any> {
        const req = `${environment.ENDPOINTS.NOTES_URL}/${noteId}`;
        return this.httpClient.get(req);
    }

    /**
     * add new note
     * @param newNote new note
     * @returns 
     */
    createNote(newNote: note): Observable<any> {
        const req = `${environment.ENDPOINTS.NOTES_URL}`;
        return this.httpClient.post(req, newNote);
    }

    /**
     * edit note
     * @param updatedNote updated note
     * @returns 
     */
    updateNote(updatedNote: note): Observable<any> {
        const req = `${environment.ENDPOINTS.NOTES_URL}/${updatedNote.id}`;
        return this.httpClient.put(req, updatedNote);
    }

    /**
     * delete note
     * @param noteId 
     * @returns 
     */
    deleteNote(noteId: number): Observable<any> {
        const req = `${environment.ENDPOINTS.NOTES_URL}/${noteId}`;
        return this.httpClient.delete(req);
    }
}
