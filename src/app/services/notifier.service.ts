import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { note } from '../shared/models/note.model';
@Injectable({
  providedIn: 'root'
})
export class NotifierService {
  private deletedNote = new Subject<any>();
  deletedNote$ = this.deletedNote.asObservable();

  private addedNote = new Subject<any>();
  addedNote$ = this.addedNote.asObservable();

  private editedNote = new Subject<any>();
  editedNote$ = this.editedNote.asObservable();

  /**
   * on delete note
   * @param note note
   */
  deletedNoteFunc(noteId: Number): void {
    this.deletedNote.next(noteId);
  }

  /**
   * on add note
   * @param note note
   */
  addedNoteFunc(note: note): void {
    this.addedNote.next(note);
  }

  /**
   * on edit note
   * @param note note
   */
  editedNoteFunc(note: note): void {
    this.editedNote.next(note);
  }


}
