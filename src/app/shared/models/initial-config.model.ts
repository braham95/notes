export class InitalConfig {
    page: number = 0; 
    size: number = 20; 
    sort: string = 'creationDate';
    dir: string = 'desc'; 
    search?: string = '';
}