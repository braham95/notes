export class note {
    id: number = 1;
    title: string = "note title";
    content: string = "note description";
    creationDate: string = new Date().toString();
    updateDate: string = new Date().toString();
    saveDate?: string | null = null;
}
