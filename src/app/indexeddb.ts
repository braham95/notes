import { DBConfig } from "ngx-indexed-db";

export const dbConfig: DBConfig  = {
    name: 'noteApp',
    version: 1,
    objectStoresMeta: [{
      store: 'notes',
      storeConfig: { keyPath: 'id', autoIncrement: true },
      storeSchema: [
        { name: 'title', keypath: 'title', options: { unique: false } },
        { name: 'content', keypath: 'content', options: { unique: false } },
        { name: 'creationDate', keypath: 'creationDate', options: { unique: false } },
        { name: 'updateDate', keypath: 'updateDate', options: { unique: false } },
        { name: 'saveDate', keypath: 'saveDate', options: { unique: false } }
      ]
    }]
  };